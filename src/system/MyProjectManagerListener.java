package system;

import com.intellij.ide.RecentProjectsManagerBase;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.ProjectManagerListener;
import com.sun.net.httpserver.HttpHandler;
import org.jetbrains.annotations.NotNull;

public class MyProjectManagerListener implements ProjectManagerListener {

    Server server;

    public void projectOpened(Project project) {

        //Notification.notify("projectOpened" + project.toString());
        StatusBar statusBar = new StatusBar(project);
        HttpHandler handler = new Handler(project);
        this.server = new Server(handler, statusBar);

    }

    public void projectClosed(Project project) {
        this.server.closeServer();
    }

}