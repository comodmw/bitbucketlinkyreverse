package system;

import com.intellij.openapi.project.Project;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.net.URI;

public class Handler implements HttpHandler {

    private Project project;

    public Handler(Project project) {
        this.project = project;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        Server.Handler.setCors(httpExchange);

        URI requestURI = httpExchange.getRequestURI();
        String query = requestURI.getQuery();

        Editor editor = new Editor(this.project);
        editor.jumpToLine(query, 0);

        Server.Handler.response(httpExchange);

        return;

    }
}
